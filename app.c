#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <time.h>
#include <stdbool.h>
#define isCompatible(x, type) _Generic(x, type: true, default: false)
///product ids 2134 2342
FILE *file1, *file2, *file3;

///business methods
void welcomePage();
void userLogin();
void dashboard(int a);
void dashboardHandler(int role,int action);

void createEmployee();
void deleteEmployee();
void viewEmployees();
void dailySellReport();
void sellReport();
void viewProducts();
void employeeDailyWorkHour();
void employeeWeeklyWorkHour();
void employeeMonthlyWorkHour();

void placeOrder();
void myOrders();
void punchInPunchOut(int dashb);

void completeOrder();

///utility methods
void replace(char *s);
void createOwner();
void loadProducts();
char consPass[15];
char* getCurrentDate();
int getCurrentDay();
int getCurrentMonth();
char* dateBuilder(int day);
char* dateBuilder2(int day);
int getDateMatcher(char *date);
char* inputPassword();

struct User {
	char name[50];
	char phone[20];
    char pass[15];
    char role[10];
};
struct User user1,user2;
struct Product {
    char name[20];
    long long proID;
    int qty;
    float uniquePrice;
};
struct Product product1,product2;

struct Order {
    int orderId;
    char empId[15];
    char date[12];
    long long proID;
    int qty;
    float totalPrice;
    char status[10];
    int tableNum;
};
struct Order order1,order2;

struct DailyWork {
    char employee[15];
    char date[12];
    long long punchin;
    long long punchout;
    long long totalhours;
};
struct DailyWork dwork1,dwork2,dwork3;
///2342,2134 have to add: employee weekly hours and monthly hours.
void main() {

    ///loadProducts();
    ///createOwner();
    ///printf("%d",getRandNum());
    welcomePage();
    userLogin();
    printf("\n\n\n");
}

void welcomePage(){
	printf("\n      ============================================================");
	printf("\n      +                                                          +");
	printf("\n      +      Welcome to POS management system                    +");
	printf("\n      +                                                          +");
	printf("\n      ============================================================");
	fflush(stdin);
}

void replace(char *s){
    s[strcspn(s, "\n")]='\0';
}

void userLogin(){
    char pass[15];
    file1=fopen("resources/user.dll","r");
    printf("\n\n      Enter your password for getting started : ");
    readagain:
    fflush(stdin);
    strcpy(pass,inputPassword());
    int checker = 0;
    while(fread(&user1,sizeof(user1),1,file1)) {
        replace(user1.pass);
        if(stricmp(pass,user1.pass)==0){
            replace(user1.role);
            checker = 1;
            strcpy(consPass,user1.pass);
            if(stricmp(user1.role,"admin")==0){
                fclose(file1);
                dashboard(0);
            } else if(stricmp(user1.role,"employee")==0){
                fclose(file1);
                dashboard(1);
            } else {
                fclose(file1);
                dashboard(2);
            }
        }
	}
	if (!checker) {
        printf("\n      Wrong password enter valid password : ");
        fseek(file1, 0, SEEK_SET); ///move pointing to up
        goto readagain;
	}
	fclose(file1);
}

void dashboard(int role){
    int navigator;
    switch (role){
        case 0:
            printf("\n      *=====================Admin Dashboard======================*");
            printf("\n      *   1.Create Employee                                      *");
            printf("\n      *   2.Delete Employee                                      *");
            printf("\n      *   3.View Employees                                       *");
            printf("\n      *   4.Order Reports Daily                                  *");
            printf("\n      *   5.Order Reports All                                    *");
            printf("\n      *   6.Products List                                        *");
            printf("\n      *   7.Employee Daily Hour                                  *");
            printf("\n      *   8.Employee Weekly Hours                                *");
            printf("\n      *   9.Employee Monthly Hours                               *");
            printf("\n      *   0.Quit                                                 *");
            printf("\n      *==========================================================*");
            printf("\n      Choose your required option : ");
            scanf("%d",&navigator);
            dashboardHandler(0,navigator);
            break;
        case 1:
            printf("\n      *==================Employee Dashboard======================*");
            printf("\n      *   1.Place order                                          *");
            printf("\n      *   2.My Orders                                            *");
            printf("\n      *   3.Punch In/Punch Out                                   *");
            printf("\n      *   0.Quit                                                 *");
            printf("\n      *==========================================================*");
            printf("\n      Choose your required option : ");
            scanf("%d",&navigator);
            dashboardHandler(1,navigator);
            break;
        default:
            printf("\n      *===================Chef Dashboard=========================*");
            printf("\n      *   1.Current order list->Complete order                   *");
            printf("\n      *   2.Punch In/Punch Out                                   *");
            printf("\n      *   0.Quit                                                 *");
            printf("\n      *==========================================================*");
            printf("\n      Choose your required option : ");
            scanf("%d",&navigator);
            dashboardHandler(2,navigator);
            break;
    }
}

void dashboardHandler(int role,int action){
  if (action==0){
        userLogin();
  }
  switch (role){
    case 0 :
        if (action==1) createEmployee();
        else if(action==2) deleteEmployee();
        else if(action==3) viewEmployees();
        else if(action==4) dailySellReport();
        else if(action==5) sellReport();
        else if(action==6) viewProducts();
        else if(action==7) employeeDailyWorkHour();
        else if(action==8) employeeWeeklyWorkHour();
        else if(action==9) employeeMonthlyWorkHour();
        else
            printf("\n      Oh sorry! Invalid input\n");
            dashboard(0);
        break;
    case 1 :
        if (action==1) placeOrder();
        else if(action==2) myOrders();
        else if(action==3) punchInPunchOut(1);
        else
            printf("\n      Oh sorry! Invalid input\n");
            dashboard(1);
        break;
    default :
        if (action==1) completeOrder();
        else if(action==2) punchInPunchOut(2);
        else {
            printf("\n      Oh sorry! Invalid input\n");
            dashboard(2);
        }
        break;
  }
}

void createEmployee(){
    file1=fopen("resources/user.dll","a");
    file2=fopen("resources/user.dll","r");
    fflush(stdin);
    printf("\n      Enter Name: ");
    fgets(user1.name, sizeof(user1.name), stdin);
    fflush(stdin);
    printf("\n      Enter phone: ");
    fgets(user1.phone, sizeof(user1.phone), stdin);
    fflush(stdin);
    printf("\n      Enter pass: ");
    label:
    fgets(user1.pass, sizeof(user1.pass), stdin);
    fflush(stdin);
    while(fread(&user2,sizeof(user2),1,file2)) {
        replace(user2.pass);replace(user1.pass);
        if(stricmp(user1.pass,user2.pass)==0){
           printf("\n      Password already taken, enter unique password: ");
           fseek(file2, 0, SEEK_SET);
           goto label;
        }
	}
    printf("\n      Enter role: ");
    fgets(user1.role, sizeof(user1.role), stdin);
    fwrite(&user1,sizeof(user1),1,file1);
    printf("\n      User created with password %s\n",user1.pass);
    fclose(file1); fclose(file2);
    fflush(stdin);
    dashboard(0);
}
void deleteEmployee(){
    fflush(stdin); char pass[15];
    file1=fopen("resources/user.dll","r");
    file2=fopen("resources/temp.dll","w");
    printf("\n      Enter password to delete an employee: ");
    scanf("%[^\n]",pass);
    int deleted=0;
    while(fread(&user1,sizeof(user1),1,file1)) {
        replace(user1.pass);
        if(stricmp(user1.pass,pass)!=0){
            fwrite(&user1,sizeof(user1),1,file2);
        }else{
            deleted=1;
            replace(user1.name);
            printf("\n      User *%s* deleted with password *%s*\n",user1.name,user1.pass);
        }
	}
	if(deleted==0){printf("\n      Error:NotFound   Msg:There is no user with password *%s*\n",pass);}
	fclose(file1);
	fclose(file2);
	fflush(stdin);
	remove("resources/user.dll");
	rename("resources/temp.dll","resources/user.dll");
	fflush(stdin);
	dashboard(0);
}
void viewEmployees(){
    fflush(stdin);
    file1=fopen("resources/user.dll","r");
    fflush(stdin);
    printf("\n      =================================================");
    printf("\n      Name        Phone          Password    Role      ");
    printf("\n      =================================================");
    while(fread(&user1,sizeof(user1),1,file1)) {
        replace(user1.name); replace(user1.pass); replace(user1.phone); replace(user1.role);
        printf("\n      %-12s%-15s%-12s%-10s",user1.name,user1.phone,user1.pass,user1.role);
	}
	printf("\n");
    fclose(file1);
    fflush(stdin);
    dashboard(0);
}
void dailySellReport(){
    fflush(stdin);
    file1=fopen("resources/orders.dll","r");
    char date[15];
    time_t s, val = 1;
    struct tm* current_time;
    s = time(NULL);
    current_time = localtime(&s);
    strcpy(date,getCurrentDate());
    printf("\n      ===========================================================");
    printf("\n      OrderId     Employee       ProductId   Quantity  TotalPrice");
    printf("\n      ===========================================================");
    while(fread(&order1,sizeof(order1),1,file1)) {
       if(stricmp(order1.date,date)==0)
            printf("\n      %-12d%-15s%-12lld%-10d%10.2f",order1.orderId,order1.empId,order1.proID,
               order1.qty,order1.totalPrice);
	}
	printf("\n");
	fflush(stdin);
    fclose(file1);
    fflush(stdin);
    dashboard(0);
}
///sell report all
void sellReport(){
    fflush(stdin);
    file1=fopen("resources/orders.dll","r");
    printf("\n      =======================================================================");
    printf("\n      OrderId     Date        Employee       ProductId   Quantity  TotalPrice");
    printf("\n      =======================================================================");
    while(fread(&order1,sizeof(order1),1,file1)) {
            printf("\n      %-12d%-12s%-15s%-12lld%-10d%10.2f",order1.orderId,order1.date,order1.empId,order1.proID,
               order1.qty,order1.totalPrice);
	}
	printf("\n");
	fflush(stdin);
    fclose(file1);
    fflush(stdin);
    dashboard(0);
}
void viewProducts(){
    fflush(stdin);
    file1=fopen("resources/products.dll","r");
    fflush(stdin);
    printf("\n      =====================================================");
    printf("\n      Name              ProductID   Quantity  UniquePrice  ");
    printf("\n      =====================================================");
    while(fread(&product1,sizeof(product1),1,file1)) {
        replace(product1.name); ///replace(product1.pass); replace(product1.phone); replace(product1.role);
        printf("\n      %-18s%-12lld%-10d%-10.2f",product1.name,product1.proID,product1.qty,product1.uniquePrice);
	}
	printf("\n");
	fclose(file1);
	fflush(stdin);
    dashboard(0);
}

void employeeDailyWorkHour(){
    file1=fopen("resources/empworkinghour.dll","r");
    printf("\n      =========================================================");
    printf("\n      Employee    Date       PunchIn    PunchOut   TotalHours  ");
    printf("\n      =========================================================");
    int hour=0,minute=0,second=0;
    char date[15];
    strcpy(date,getCurrentDate());
    while(fread(&dwork1,sizeof(dwork1),1,file1)) {
       if(stricmp(date,dwork1.date)==0){
        hour=dwork1.totalhours/(60*60);
        minute=(dwork1.totalhours-(hour*60*60))/60;
        second=(dwork1.totalhours-((hour*60*60)+(minute*60)));
        printf("\n      %-12s%-11s%-11lld%-11lld%02d:%02d:%02d",dwork1.employee,dwork1.date,dwork1.punchin,
              dwork1.punchout,hour,minute,second);
       }
	}
	printf("\n");
	fclose(file1);
    dashboard(0);
}

void employeeWeeklyWorkHour(){
    file1=fopen("resources/empworkinghour.dll","r");
    char employee[15];
    printf("\n      Enter employee id: ");
    scanf("%s",employee);
    int i=0;
    long long secs = 0;
    while(fread(&dwork1,sizeof(dwork1),1,file1)) {
       if(getDateMatcher(dwork1.date)==1 &&
          strcmp(dwork1.employee,employee)==0){
          secs = secs + dwork1.totalhours;
       }
	}
	int hour=0,minute=0,second=0;
    hour=secs/(60*60);
    minute=(secs-(hour*60*60))/60;
    second=(secs-((hour*60*60)+(minute*60)));
	printf("\n      Total work hour for last 7 days for %s : %02d:%02d:%02d\n",employee,hour,minute,second);
	fclose(file1);
    dashboard(0);
}

void employeeMonthlyWorkHour(){
    file1=fopen("resources/empworkinghour.dll","r");
    char employee[15];
    printf("\n      Enter employee id: ");
    scanf("%s",employee);
    int i=0;
    long long secs = 0;
    while(fread(&dwork1,sizeof(dwork1),1,file1)) {
       if(getDateMatcherMonth(dwork1.date)==1 &&
          strcmp(dwork1.employee,employee)==0){
          secs = secs + dwork1.totalhours;
       }
	}
	int hour=0,minute=0,second=0;
    hour=secs/(60*60);
    minute=(secs-(hour*60*60))/60;
    second=(secs-((hour*60*60)+(minute*60)));
	printf("\n      Total work hour for current month for %s : %02d:%02d:%02d\n",employee,hour,minute,second);
	fclose(file1);
    dashboard(0);
}

void placeOrder(){
    file1=fopen("resources/orders.dll","a");
    file2=fopen("resources/products.dll","r");
    fflush(stdin);
    order1.orderId = getRandNum();
    time_t s, val = 1;
    struct tm* current_time;
    s = time(NULL);
    current_time = localtime(&s);
    strcpy(order1.date,getCurrentDate());
    strcpy(order1.empId,consPass);
    printf("\n      Enter table number: ");
    scanf("%d",&order1.tableNum);
    printf("\n      Enter productId: ");
    scanf("%lld",&order1.proID);
    fflush(stdin);
    printf("\n      Enter quantity: ");
    scanf("%d",&order1.qty);
    strcpy(&order1.status,"new");
    int found=0;
    while(fread(&product1,sizeof(product1),1,file2)) {
        if(product1.proID==order1.proID){
            found=1;
            order1.totalPrice=order1.qty*product1.uniquePrice;
        }
	}
    if(found==0){
        printf("\n      Error:NotFound   Msg:The product id *%lld* is invalid\n",order1.proID);
        fclose(file1);fclose(file2);
        fflush(stdin);
        dashboard(1);
    }
    fwrite(&order1,sizeof(order1),1,file1);
    printf("\n      Successfully order placed\n        OrderId    : %d\n        EmployeeId : %s\
           \n        ProductId  : %lld\n        Quantity   : %d\n        TotalPrice : %.2f\n",order1.orderId,order1.empId,
           order1.proID,order1.qty,order1.totalPrice);
    fflush(stdin);
    fclose(file1);fclose(file2);
    dashboard(1);
}

void myOrders(){
    fflush(stdin);
    file1=fopen("resources/orders.dll","r");
    fflush(stdin);
    printf("\n      ===========================================================");
    printf("\n      OrderId     Employee       ProductId   Quantity  TotalPrice");
    printf("\n      ===========================================================");
    while(fread(&order1,sizeof(order1),1,file1)) {
       if(stricmp(order1.empId,consPass)==0)
            printf("\n      %-12d%-15s%-12lld%-10d%10.2f",order1.orderId,order1.empId,order1.proID,
               order1.qty,order1.totalPrice);
	}
	printf("\n");
	fclose(file1);
	fflush(stdin);
    dashboard(1);
}

void completeOrder(){
    fflush(stdin);
    file1=fopen("resources/orders.dll","r");
    fflush(stdin);
    printf("\n      ===========================================================");
    printf("\n      OrderId     ProductId  Quantity  Status    TableNumber     ");
    printf("\n      ===========================================================");
    int count=0;
    while(fread(&order1,sizeof(order1),1,file1)) {
       if(stricmp(order1.status,"new")==0){
            printf("\n      %-12d%-11lld%-10d%-10s%d",order1.orderId,order1.proID,
               order1.qty,order1.status,order1.tableNum);
               count++;
       }
	}
	printf("\n");
	fclose(file1);
	fflush(stdin);
    if(count>0){
        int id;
        printf("\n      Enter orderId and complete an order otherwise 0: ");
        scanf("%d",&id);
        file1=fopen("resources/orders.dll","r");
        file2=fopen("resources/temp.dll","w");
        fflush(stdin);
        while(fread(&order1,sizeof(order1),1,file1)) {
            if(id==order1.orderId){
                strcpy(order1.status,"completed");
                printf("\n      Completed order number*%d*\n",id);
            }
            fwrite(&order1,sizeof(order1),1,file2);
        }
        printf("\n");
        fclose(file1);fclose(file2);
        remove("resources/orders.dll");
        rename("resources/temp.dll","resources/orders.dll");
    }else{
        printf("\n      There is no new order!!\n");
    }
    dashboard(2);
}

///www.codevscolor.com/c-print-current-time-day-month-year
void punchInPunchOut(int dashb){
    dwork1 = (const struct DailyWork){ 0 };
    dwork2 = (const struct DailyWork){ 0 };
    dwork3 = (const struct DailyWork){ 0 };
    time_t s, val = 1;
    struct tm* current_time;
    s = time(NULL);
    current_time = localtime(&s);
    file1=fopen("resources/empworkinghour.dll","a+");
    file2=fopen("resources/empworkinghour.dll","r");
    strcpy(dwork1.employee,consPass);
    strcpy(dwork1.date,getCurrentDate());
    int check=0;
    while(fread(&dwork2,sizeof(dwork2),1,file2)) {
       if(stricmp(dwork2.date,dwork1.date)==0 &&
          stricmp(dwork2.employee,consPass)==0){
                if(dwork2.punchout==0){
                    check=2;
                    fseek(file2, 0, SEEK_SET);
                    break;
                }
            check=1;
            break;
       }
	}
	if(check==2){
        file3=fopen("resources/temp.dll","a+");
        printf("\n      Successfully punched out\n");
        while(fread(&dwork3,sizeof(dwork3),1,file2)) {
            if(stricmp(dwork3.date,dwork1.date)==0 &&
               stricmp(dwork3.employee,consPass)==0){
                dwork3.punchout=(current_time->tm_hour*60*60)+(current_time->tm_min*60)+current_time->tm_sec;
                dwork3.totalhours=(long long)(dwork3.punchout-dwork3.punchin);
            }
            fwrite(&dwork3,sizeof(dwork3),1,file3);
        }
        fflush(stdin);
        fclose(file1);fclose(file2);fclose(file3);
        fflush(stdin);
        remove("resources/empworkinghour.dll");
        fflush(stdin);
        rename("resources/temp.dll", "resources/empworkinghour.dll");
        fflush(stdin);
	}else if(check==0){
	    printf("\n      Successfully punched in\n");
	    fflush(stdin);
        dwork1.punchin=(current_time->tm_hour*60*60)+(current_time->tm_min*60)+current_time->tm_sec;
        fwrite(&dwork1,sizeof(dwork1),1,file1);
        fclose(file1);fclose(file2);fclose(file3);
	}else if(check==1){
	    printf("\n      Punched in Punched out completed for today\n");
	    fclose(file1);fclose(file2);fclose(file3);
	}
    dashboard(dashb);
}

char* inputPassword(){
    static char str[15];
    char c=' ';
    int i=0;
    while (i<=9){
        str[i]=getch();
        c=str[i];
        if(c==13) break;
        else printf("*");
        i++;
    }
    str[i]='\0';
    i=0;
    printf("\n");
    return str;
}

int randNum(){
    time_t t;
    srand((unsigned) time(&t));
    return rand();
}
int getRandNum() {
 randNum();
 return rand() % (100000 - 1 + 1) + 1;
}

char* getCurrentDate(){
    time_t s, val = 1;
    struct tm* current_time;
    s = time(NULL);
    current_time = localtime(&s);
    static char date[15];
    char temp[5];
    itoa(current_time->tm_year+1900,temp,10);
    strcpy(date,temp);
    itoa(current_time->tm_mon+1,temp,10);
    strcat(date, "-");
    strcat(date, temp);
    itoa(current_time->tm_mday,temp,10);
    strcat(date,"-");
    strcat(date,temp);
    return date;
}
int getDateMatcher(char *date){
    time_t s, val = 1;
    struct tm* current_time;
    s = time(NULL);
    current_time = localtime(&s);
    int day=getCurrentDay();
    for(int i=0;i<7;i++){
       if(stricmp(date,dateBuilder(i))==0){
          return 1;
       }
       day--;
       if(day==0)
          return 0;
    }
    return 0;
}
int getDateMatcherMonth(char *date){
    time_t s, val = 1;
    struct tm* current_time;
    s = time(NULL);
    current_time = localtime(&s);
    int day = getCurrentDay();
    for(;day>0;day--){
       if(stricmp(date,dateBuilder2(day))==0){
          return 1;
       }
    }
    return 0;
}
char* dateBuilder(int day){
    time_t s, val = 1;
    struct tm* current_time;
    s = time(NULL);
    current_time = localtime(&s);
    static char date[15];
    char temp[5];
    itoa(current_time->tm_year+1900,temp,10);
    strcpy(date,temp);
    itoa(current_time->tm_mon+1,temp,10);
    strcat(date, "-");
    strcat(date, temp);
    itoa(current_time->tm_mday-day,temp,10);
    strcat(date,"-");
    strcat(date,temp);
    return date;
}

char* dateBuilder2(int day){
    time_t s, val = 1;
    struct tm* current_time;
    s = time(NULL);
    current_time = localtime(&s);
    static char date[15];
    char temp[5];
    itoa(current_time->tm_year+1900,temp,10);
    strcpy(date,temp);
    itoa(current_time->tm_mon+1,temp,10);
    strcat(date, "-");
    strcat(date, temp);
    itoa(day,temp,10);
    strcat(date,"-");
    strcat(date,temp);
    return date;
}

int getCurrentDay(){
    time_t s, val = 1;
    struct tm* current_time;
    s = time(NULL);
    current_time = localtime(&s);
    return current_time->tm_mday;
}
int getCurrentMonth(){
    time_t s, val = 1;
    struct tm* current_time;
    s = time(NULL);
    current_time = localtime(&s);
    return current_time->tm_mon+1;
}

void createOwner(){
    file1=fopen("resources/user.dll","a");
    fgets(user1.name, sizeof(user1.name), stdin);
    fgets(user1.phone, sizeof(user1.phone), stdin);
    fgets(user1.pass, sizeof(user1.pass), stdin);
    fgets(user1.role, sizeof(user1.role), stdin);
    fwrite(&user1,sizeof(user1),1,file1);
    fclose(file1);
    fflush(stdin);
}
void loadProducts(){
    file1=fopen("resources/products.dll","w");
    struct Product arrProduct[10] = {
                            {"Pizza",1,23,32.2},
                            {"Mixed Pakora",2,24,235.4},
                            {"Samosas",3,24,34.4},
                            {"Omelette Wrap",4,24,76.4},
                            {"Aloo Paratha",5,24,23.4},
                            {"Lassi",6,24,62.4},
                            {"Naan",7,24,567.4},
                            {"Poutine",8,24,97.4},
                            {"Roti Wrap-Beef",9,24,23.4},
                            {"Roti Wrap-Paneer",10,24,56.4}
                        };
    for (int i=0;i<10;i++){
        fwrite(&arrProduct[i],sizeof(product1),1,file1);
        fflush(stdin);
    }
    fclose(file1);
    fflush(stdin);
}








